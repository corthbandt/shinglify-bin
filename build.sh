

# CGO_ENABLED=0 go build -v -ldflags="-X 'gitlab.com/corthbandt/shinglify/shinglify.VersionString=`git rev-parse --short=8 HEAD`' -X 'gitlab.com/corthbandt/shinglify/shinglify.BuildDate=`date --utc +"%Y%m%d%H%M%S"`'"  -o ./pg3server pgserver.go
cd ../shinglify

vdate=`git log -1 --format="%at" | xargs -I{} date -d @{} +%Y%m%d-%H%M%S`
vname=`git describe --tags --abbrev=8`
bdate=`date --utc +"%Y%m%d%H%M%S"`

vid="${vdate}_${vname}"

go build -v -ldflags="-X 'gitlab.com/corthbandt/shinglify/shinglify.VersionString=${vid}' -X 'gitlab.com/corthbandt/shinglify/shinglify.BuildDate=${bdate}'"  -o ../shinglify-bin/shinglify_linux_amd64 cmd/cmd.go

cd ../shinglify-bin

cp -f ../shinglify/README.md ./
cp -f ../shinglify/LICENSE ./

echo "Version: ${vdate}_${vname}"

git add .
git commit -m "build $vid"
git tag -fa $vid -m "build $vid"
git tag -fa latest -m "build $vid"

git push -f --tags
